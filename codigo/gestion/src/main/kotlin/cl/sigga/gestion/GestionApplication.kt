package cl.sigga.gestion

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GestionApplication

fun main(args: Array<String>) {
    runApplication<GestionApplication>(*args)
}
