package cl.sigga.gestion.controllers

import cl.sigga.gestion.data.Respuesta
import cl.sigga.gestion.services.LoginService
import cl.sigga.gestion.utils.ERROR_VALIDACION
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
open class LoginController {

    @GetMapping("/")
    open fun login(): String = "login"
}


@RestController
open class LoginRestController {

    @Autowired
    lateinit var ls: LoginService

    @PostMapping(path = arrayOf("/validar2"), consumes = arrayOf(MediaType.ALL_VALUE))
    @CrossOrigin(origins = ["*"])
    @ResponseBody
    open fun validar2(@RequestParam("usuario") usuario: String, @RequestParam("clave") clave: String): ResponseEntity<Respuesta> {

        var respuesta: Respuesta = Respuesta("ok")

        when (ls.validar(usuario, clave)) {
            ERROR_VALIDACION.USUARIO -> respuesta = Respuesta("usuario")
            ERROR_VALIDACION.CLAVE -> respuesta = Respuesta("clave")
            ERROR_VALIDACION.AMBAS -> respuesta = Respuesta("nok")
        }

        return ResponseEntity.ok(respuesta)
    }

    @PostMapping(path = arrayOf("/validar"), consumes = arrayOf(MediaType.ALL_VALUE))
    @CrossOrigin(origins = ["*"])
    @ResponseBody
    open fun validar(@RequestParam("usuario") usuario: String, @RequestParam("clave") clave: String): ResponseEntity<Respuesta> {

        var respuesta: Respuesta = ls.validar2(usuario,clave)
        return ResponseEntity.ok(respuesta)
    }
}

