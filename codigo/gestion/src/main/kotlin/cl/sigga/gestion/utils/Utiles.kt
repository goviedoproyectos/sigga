package cl.sigga.gestion.utils

enum class ERROR_VALIDACION {
    USUARIO,
    CLAVE,
    AMBAS,
    OK
}