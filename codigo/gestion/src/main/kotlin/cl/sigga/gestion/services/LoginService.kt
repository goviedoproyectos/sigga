package cl.sigga.gestion.services

import cl.sigga.gestion.data.Respuesta
import cl.sigga.gestion.utils.ERROR_VALIDACION
import cl.sigga.gestion.utils.ERROR_VALIDACION.*
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

@Service
open class LoginService {

    /**
     * Valida nombre de usuario y clave de entrada
     */
    open fun validar(usuario: String, clave: String): ERROR_VALIDACION {

        if (!usuario.equals("a") && !clave.equals("a")) {
            return AMBAS;
        }

        if (!usuario.equals("a")) {
            return USUARIO;
        }

        if (!clave.equals("a")) {
            return CLAVE;
        }
        return OK;
    }


    /**
     *
     */
    fun validar2(usuario: String, clave: String): Respuesta {

        val url = "http://localhost:7020/validar"

        var mapa: MultiValueMap<String, String> = LinkedMultiValueMap<String, String>()
        mapa.add("usuario", usuario)
        mapa.add("clave", clave)

        val r: Respuesta = RestTemplate().postForObject(url, mapa, Respuesta::class.java)!!

        return r
    }

}


