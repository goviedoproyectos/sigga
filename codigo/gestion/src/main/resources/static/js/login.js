$(document).ready(function () {
    /*****************************
    * Definicion de funciones
    ******************************/

    // Verifica nombre de usuario y clave
    var login = function() {
        var datos = $("#formulario").serialize();

        $.ajax({
            type: 'post',
            url:'http://localhost:7080/validar',
            data: datos,
            success: function(data) {

                // Posibles respuestas son:
                // ok: correctas usuario y clave,
                // nok: Ambas incorrectas,
                // usuario: usuario no reconocido,
                // clave: clave no reconocida

                console.dir(data);
                var r = data.respuesta;
                switch(r) {
                    case "ok": $("#usuario").removeClass("is-invalid"); $("#clave").removeClass("is-invalid"); alert('Credenciales Validas');
                    break;
                    case "usuario": $("#usuario").addClass("is-invalid"); $("#clave").removeClass("is-invalid"); $("#clave").addClass("is-valid");
                    break;
                    case "clave": $("#clave").addClass("is-invalid"); $("#usuario").removeClass("is-invalid"); $("#usuario").addClass("is-valid");
                    break;
                    case "nok": $("#usuario").addClass("is-invalid"); $("#clave").addClass("is-invalid");
                    break;
                    default:
                }
            },
            error: function(jqXhr, error) {
                console.dir(jqXhr);
                console.dir(error);
            }
        });
    };

    /*****************************
    * Definicion de eventos
    ******************************/

    $("#formulario").submit(function(event) {
        event.preventDefault();
        login();
    });
});

