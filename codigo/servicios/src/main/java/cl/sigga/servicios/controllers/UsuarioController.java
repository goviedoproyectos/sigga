package cl.sigga.servicios.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.sigga.servicios.dtos.RespuestaUsuarioDTO;
import cl.sigga.servicios.services.UsuarioService;

@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioService us;
	
	@PostMapping("/validar")
	public ResponseEntity<RespuestaUsuarioDTO> validar(@RequestParam("usuario") String usuario, @RequestParam("clave") String clave) {
		RespuestaUsuarioDTO r = us.validacionUsuario(usuario, clave);
		return ResponseEntity.ok(r);
	}

}
