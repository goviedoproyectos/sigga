package cl.sigga.servicios.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.sigga.servicios.entities.UsuarioEntity;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer> {

	UsuarioEntity findByUsuario(String usuario);
	UsuarioEntity findByClave(String clave);
}
