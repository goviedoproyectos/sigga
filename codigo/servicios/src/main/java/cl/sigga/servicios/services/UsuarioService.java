package cl.sigga.servicios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.sigga.servicios.dtos.RespuestaUsuarioDTO;
import cl.sigga.servicios.entities.UsuarioEntity;
import cl.sigga.servicios.repositories.UsuarioRepository;
import cl.sigga.servicios.utils.ErrorValidacion;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository ur;

	/**
	 * Los valores que retorna en la respuesta son:
	 * ok: Usuario Autenticado 
	 * nok: Nombre de usuario y clave incorrectas.
	 * usuario: nombre de usuario incorrecto. 
	 * clave: clave incorrecta
	 * 
	 * @param usuario
	 * @param clave
	 * @return
	 */
	public RespuestaUsuarioDTO validacionUsuario(String usuario, String clave) {
		
		RespuestaUsuarioDTO r = new RespuestaUsuarioDTO();
		
		switch(validar(usuario, clave))
		{
			case AMBAS: r.setRespuesta("nok");
			break;
			case USUARIO: r.setRespuesta("usuario");
			break;
			case CLAVE: r.setRespuesta("clave");
			break;
			case OK: r.setRespuesta("ok");
			break;
		}
		
		return r;
	}
	
	/**
	 * Devuelve un enum con: AMBAS: Tanto la clave como el usuario es invalido
	 * USUARIO: El nombre de usuario no lo reconozco CLAVE: La clave es incorrecta
	 * OK: Usuario autenticado.
	 * 
	 * @param usuario
	 *            nombre de usuario
	 * @param clave
	 *            clave o password del usuario
	 * @return
	 */
	private ErrorValidacion validar(String usuario, String clave) {

		UsuarioEntity u = ur.findByUsuario(usuario);
		UsuarioEntity c = ur.findByClave(clave);

		if (u == null && c == null) {
			return ErrorValidacion.AMBAS;
		}

		if (u == null) {
			return ErrorValidacion.USUARIO;
		}

		if (c == null) {
			return ErrorValidacion.CLAVE;
		}
		return ErrorValidacion.OK;

	}
}
