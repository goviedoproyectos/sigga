package cl.sigga.servicios.dtos;

import java.io.Serializable;

/**
 * Respuesta de Usuario si es valido o no.
 * Posibles respuestas son:
 * 
 * ok: Usuario Autenticado
 * nok: Nombre de usuario y clave incorrectas.
 * usuario: nombre de usuario incorrecto.
 * clave: clave incorrecta
 * @author goviedo
 *
 */
public class RespuestaUsuarioDTO implements Serializable {
	
	private String respuesta;

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	

}
