package cl.sigga.servicios.utils;

public enum ErrorValidacion {
	USUARIO,
	CLAVE,
	AMBAS,
	OK
}
