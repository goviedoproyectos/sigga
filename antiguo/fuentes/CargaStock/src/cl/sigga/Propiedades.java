package cl.sigga;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * El archivo de propiedades debe estar ubicado en la misma ruta que el jar
 * @author goviedo
 *
 */
public class Propiedades {
	
	private String nombreBd;
	private String usuario;
	private String password;
	private int puerto;
	private String ip;
	
	public Propiedades() throws IOException {
		Properties prop = new Properties();
		InputStream is = null;
		
		is = new FileInputStream(Constantes.ARCHIVO_PROPIEDADES);
		//is = new FileInputStream("bd.properties"); //Depuración
		prop.load(is);
		
		this.nombreBd = prop.getProperty("servidor.nombrebd");
		this.usuario = prop.getProperty("servidor.usuario");
		this.password = prop.getProperty("servidor.password");
		this.puerto = new Integer(prop.getProperty("servidor.puerto")).intValue();
		this.ip = prop.getProperty("servidor.ip");
	}

	public String getNombreBd() {
		return nombreBd;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getPassword() {
		return password;
	}

	public int getPuerto() {
		return puerto;
	}

	public String getIp() {
		return ip;
	}
}
