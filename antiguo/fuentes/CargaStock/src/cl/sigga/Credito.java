package cl.sigga;

import java.util.Date;

import com.ebay.xcelite.annotations.Column;

/**
 * Datos de una operacion IFI
 * 
 * @author Gonzalo Oviedo
 */
public class Credito {

	
	
	@Column(name = "OpIFI")
	private int opIFI;
	@Column(name = "FechaPago")
	private Date fechaPago;
	@Column(name = "FechaVencimiento")
	private Date fechaVencimiento;
	@Column(name = "CuotaQcCancela")
	private int cuotaQueCancela;
	@Column(name = "MontoPagadoMO")
	private long montoPago;
	@Column(name = "SaldoInsolutoMO")
	private long saldoInsolutoMO;
	@Column(name = "CodigoMora")
	private int codigoMora;

	public long getMontoPago() {
		return montoPago;
	}

	public void setMontoPago(long montoPago) {
		this.montoPago = montoPago;
	}

	public int getOpIFI() {
		return opIFI;
	}

	public void setOpIFI(int opIFI) {
		this.opIFI = opIFI;
	}

	public int getCuotaQueCancela() {
		return cuotaQueCancela;
	}

	public void setCuotaQueCancela(int cuotaQueCancela) {
		this.cuotaQueCancela = cuotaQueCancela;
	}

	public long getSaldoInsolutoMO() {
		return saldoInsolutoMO;
	}

	public void setSaldoInsolutoMO(long saldoInsolutoMO) {
		this.saldoInsolutoMO = saldoInsolutoMO;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public int getCodigoMora() {
		return codigoMora;
	}

	public void setCodigoMora(int codigoMora) {
		this.codigoMora = codigoMora;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	@Override
	public String toString() {
		String salida = "";
		salida += "Descripcion Operacion";
		salida += "\n----------------------";

		salida += "\nOperacion IFI:" + this.opIFI;
		salida += "\nFecha de Pago:" + this.fechaPago;
		salida += "\nFecha de Vencimiento:" + this.fechaVencimiento;
		salida += "\nCuta que Cancela:" + this.cuotaQueCancela;
		salida += "\nMonto de pago:" + this.montoPago;
		salida += "\nSaldo Insoluto MO:" + this.saldoInsolutoMO;
		salida += "\nCodigo de Mora:" + this.codigoMora;
		salida += "\n-.-";

		return salida;
	}

}
