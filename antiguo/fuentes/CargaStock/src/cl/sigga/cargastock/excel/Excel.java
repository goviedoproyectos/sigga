package cl.sigga.cargastock.excel;

import java.io.File;
import java.util.Collection;

import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.reader.SheetReader;
import com.ebay.xcelite.sheet.XceliteSheet;

import cl.sigga.Credito;

/**
 * Genera una lectura en Excel.
 * Utiliza la libreria xcelite de e-bay.
 * @author goviedo
 */
public class Excel {

	private Collection<Credito> c = null;
	private String archivo;
	private String hoja;

	/**
	 * Constructor
	 * @param archivo El archivo con el path completo donde ir a buscarlo
	 * @param hoja La hoja del excel que se debe leer.
	 */
	public Excel(String archivo, String hoja) {
		this.archivo = archivo;
		this.hoja = hoja;
	}
	
	/**
	 * Permite la lectura y genera una Coleccion de Credito.
	 */
	private void leer() throws Exception {
		Xcelite xcelite = new Xcelite(new File(archivo));
		XceliteSheet sheet = xcelite.getSheet(hoja);
		SheetReader<Credito> reader = sheet.getBeanReader(Credito.class);
		c = reader.read();
	}

	/**
	 * Obtiene la lectura del excel.
	 * @return Coleccion de Credito
	 */
	public Collection<Credito> getHojaExcel() throws Exception {
		this.leer();
		return c;
	}
	
}
