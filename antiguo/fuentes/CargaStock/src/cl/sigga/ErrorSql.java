package cl.sigga;

/**
 * Informacion relevante para error de operacion en la base de datos.
 * @author Gonzalo Oviedo L.
 */
public class ErrorSql {
	private String comentario;
	private int ultimaFilaInsertada;
	private Throwable excepcion;
	
	public ErrorSql(int ultimaFilaInsertada, Throwable excepcion, String comentario) {
		this.ultimaFilaInsertada = ultimaFilaInsertada;
		this.excepcion = excepcion;
		this.comentario = comentario;
	}
	
	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getUltimaFilaInsertada() {
		return ultimaFilaInsertada;
	}
	public void setUltimaFilaInsertada(int ultimaFilaInsertada) {
		this.ultimaFilaInsertada = ultimaFilaInsertada;
	}
	public Throwable getExcepcion() {
		return excepcion;
	}
	public void setExcepcion(Throwable excepcion) {
		this.excepcion = excepcion;
	}
	
	
}
