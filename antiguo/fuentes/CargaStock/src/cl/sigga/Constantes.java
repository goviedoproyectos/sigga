package cl.sigga;

/**
 * Constantes a nivel global de la aplicación.
 * @author Gonzalo Oviedo L.
 */
public class Constantes {
	public static final String FORMATO_FECHA = "dd-MM-yyyy";
	public static final String ARCHIVO_PROPIEDADES = "/home/sigga/sh/carga_stock/bd.properties";
	public static final String HOJA_EXCEL = "stock";

}
