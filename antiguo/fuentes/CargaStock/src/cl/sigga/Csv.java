package cl.sigga;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Lee el archivo CSV y prepara un Mapa con la operacion IFI como clave
 * 
 * @author Gonzalo Oviedo
 */
public class Csv {

	private List<Credito> operaciones = new ArrayList<Credito>();
	private List<Error> errores = new ArrayList<Error>();
	
	/**
	 * Valida si un formato de fecha es valido
	 * @author Gonzalo Oviedo
	 * @param value String con el valor a verificar
	 * @return El string pasado como parametro si esta correcto o una excepción
	 * @throws ParseException 
	 */
	public String isValidFormat(String value) throws ParseException {
        Date date = null;
        
        if(!value.isEmpty()) {
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constantes.FORMATO_FECHA);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                throw new ParseException("El formato de la fecha no parece correcto:"+value, 0);
            }
        } catch (ParseException ex) {
            throw ex;
        }
        } else {
        	value = "";
        }
        return value;
    }

	private void setOperacion(String[] datos, int numeroLinea) {

		Credito credito = new Credito();
		int opIFI = 0;
		int i = 0; //Columna
		try {
			opIFI = new Integer(datos[i++]).intValue();
		} catch (NumberFormatException ex) {
			Error e = new Error(numeroLinea, "Op IFI", "Debe ser un número entero", ex);
			errores.add(e);
		} catch (Exception ex1) {
			Error e = new Error(numeroLinea, "Op IFI", "Desconozco que pasó", ex1);
			errores.add(e);
		}

		String fechaVencimiento = "";
		try {
			fechaVencimiento = isValidFormat(datos[i++]);
		} catch (ParseException e1) {
			Error e = new Error(numeroLinea, "Fecha de Vencimiento", "El formato de la fecha debiera ser dd-MM-yyyy o la columna del excel tipo fecha", e1);
			errores.add(e);
		} catch(Exception ex) {
			Error e = new Error(numeroLinea, "Fecha de Vencimiento", "Desconozco que paso", ex);
			errores.add(e);
		}


		String fechaPago = "";
		try {
			fechaPago = isValidFormat(datos[i++]);
		} catch (ParseException e1) {
			Error e = new Error(numeroLinea, "Fecha de Pago", "El formato dela fecha debiera ser dd-MM-yyyy o la columna del excel tipo fecha", e1);
			errores.add(e);
		} catch(Exception ex) {
			Error e = new Error(numeroLinea, "Fecha de Pago", "Desconozco que paso", ex);
			errores.add(e);
		}

		int cuotaQueCancela = 0;
		try {
			if(!datos[i].isEmpty()) {
				cuotaQueCancela = new Integer(datos[i++]).intValue();
			}
		} catch (NumberFormatException ex) {
			Error e = new Error(numeroLinea, "Cuota que Cancela", "Debe ser un número entero", ex);
			errores.add(e);
		} catch (Exception ex1) {
			Error e = new Error(numeroLinea, "Cuota que Cancela", "Desconozco que paso", ex1);
			errores.add(e);
		}

		long montoPago = 0;

		try {
			if(!datos[i].isEmpty()) {
				montoPago = new Long(datos[i++]).longValue();
			}
		} catch (NumberFormatException ex) {
			Error e = new Error(numeroLinea, "Monto de pago", "Debe ser un entero/número", ex);
			errores.add(e);
		} catch (Exception ex) {
			Error e = new Error(numeroLinea, "Monto de pago", "Desconozco que paso", ex);
			errores.add(e);
		}

		long saldoInsolutoMO = 0;

		try {
			if(!datos[i].isEmpty()) {
				saldoInsolutoMO = new Long(datos[i++]).longValue();
			}
		} catch (NumberFormatException ex) {
			Error e = new Error(numeroLinea, "Saldo Insoluto MO", "Debe ser un entero/número", ex);
			errores.add(e);
		} catch (Exception ex) {
			Error e = new Error(numeroLinea, "Saldo Insoluto MO", "Desconozco que paso", ex);
			errores.add(e);
		}

		
		int codigoMora = 0;

		try {
			if(!datos[i].isEmpty()) {
				codigoMora = new Integer(datos[i++]).intValue();
			}
		} catch (NumberFormatException ex) {
			Error e = new Error(numeroLinea, "Codigo de Mora", "Las codififcaciones son 1,2,3,4,5,13,14,15", ex);
			errores.add(e);
		} catch (Exception ex) {
			Error e = new Error(numeroLinea, "Codigo de Mora", "Desconozco que paso", ex);
			errores.add(e);
		}

		
		credito.setOpIFI(opIFI);
		//credito.setFechaPago(fechaPago);
		credito.setCuotaQueCancela(cuotaQueCancela);
		credito.setSaldoInsolutoMO(saldoInsolutoMO);
		credito.setMontoPago(montoPago);
		//credito.setFechaVencimiento(fechaVencimiento);
		credito.setCodigoMora(codigoMora);
		
		operaciones.add(credito);
		
		i=0;
	}

	/**
	 * Procede a leer el archivo. Archivo debe venir con la ruta absoluta
	 * completa
	 * 
	 * @param archivo
	 *            Archivo con la ruta absoluta en donde se encuentra
	 * @return Mapa como clave el numero Operacion IFI y un arreglo con los
	 *         datos
	 * @throws IOException 
	 */
	public List<Credito> leeArchivo(String archivo) throws IOException {

		String csvFile = archivo;
		String line = "";
		String cvsSplitBy = ",";
		int numeroLinea = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				if (numeroLinea != 0) { // Se salta el encabezado
					
					// use comma as separator
					String[] datos = line.split(cvsSplitBy);
					this.setOperacion(datos, numeroLinea);
				}
				numeroLinea++;
			}

		} catch (IOException e) {
			throw e;
		}
		return this.operaciones;
	}

	/**
	 * Retorna el listao de Creditos leidos del archivo CSV
	 * 
	 * @return List de Objetos Credito
	 */
	public List<Credito> getOperaciones() {
		return this.operaciones;
	}

	/**
	 * Listado de posibles errores
	 * 
	 * @return List de Objetos Error
	 */
	public List<Error> getErrores() {
		return this.errores;
	}

}
