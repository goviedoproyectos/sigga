package cl.sigga;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Operaciones de Credito para la base de datos.
 * 
 * @author Gonzalo Oviedo L
 */
public class Operaciones {

	private Collection<Credito> operacionesCredito = new ArrayList<Credito>();
	private int cantidadRegistrosInsertados = 0;
	private int cantidadRegistrosActualizados = 0;
	private ErrorSql error = null;
	private int fila = 0;
	private Conexion conexion = null;
	private Connection c = null;

	public Operaciones(Collection<Credito> operaciones) {
		this.operacionesCredito = operaciones;

	}

	/**
	 * Inserta un comentario para informar el estado de la carga Puede ser
	 * exitosa o con errores.
	 * 
	 * @param comentario
	 *            Comentario que ira en el campo de la tabla informe_cargas
	 * @throws Exception
	 *             La excepcion del error.
	 */
	public void insertarComentarioInformeCargas(String comentario) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			conexion = new Conexion();
			c = conexion.getConexion();
			stmt = c.createStatement();

			java.sql.Date fechaHoy = new java.sql.Date(new Date().getTime());
			DateFormat df = new SimpleDateFormat("yyyyMMdd");

			System.out.println("Comentario (" + df.format(fechaHoy)+") : "+comentario);

			String sql = "update informe_cargas " + " set status = '" + comentario + "'"
					+ " where nombre_carga = 'CARGA_STOCK' " + " and fecha_carga = '" + df.format(fechaHoy) + "'";

			stmt.executeUpdate(sql);

		} catch (ClassNotFoundException | SQLException | IOException e) {
			throw e;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (SQLException e) {
				throw e;
			}
		}
	}

	/**
	 * Convierte la fecha de string en formato requerido por el usuario en su
	 * correspondiente fecha para base de datos.
	 * 
	 * @param fecha
	 * @return Date fecha
	 * @throws ParseException
	 */
	private String convierteFecha(Date fecha) throws ParseException {

		java.sql.Date sql = new java.sql.Date(fecha.getTime());

		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		return df.format(sql);

	}

	/**
	 * Inserción o actualizacion de operaciones en la tabla pagos.
	 */
	@SuppressWarnings("resource")
	public void insertarOperaciones() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			conexion = new Conexion();
			c = conexion.getConexion();

			// Seleccionamos el identificador de curse

			for (Credito credito : operacionesCredito) {

				stmt = c.createStatement();

				String sql_id_curse = "select distinct c.id_curse " + "from curse c " + "where c.id_credito = "
						+ credito.getOpIFI();

				rs = stmt.executeQuery(sql_id_curse);

				int id_curse = 0;
				while (rs.next()) {
					id_curse = rs.getInt("id_curse");
				}

				if (id_curse > 0) { // si el registro existe, actualizamos el
									// mismo.

					int existe = 0;
					String sql_existeRegistro = "select 1 as existe " + "from pagos " + "where id_curse = " + id_curse
							+ " " + "and id_credito = " + credito.getOpIFI() + " " + "and numero_cuota = "
							+ credito.getCuotaQueCancela();

					rs = stmt.executeQuery(sql_existeRegistro);

					while (rs.next()) {
						existe = rs.getInt("existe");
					}

					if (existe == 1) {

						try {
							StringBuffer sql = new StringBuffer();

							sql.append("update pagos ");
							sql.append("set ");
							sql.append("monto_pago = ");
							sql.append(credito.getMontoPago());
							sql.append(",fecha_pago = ");
							sql.append(credito.getFechaPago() == null ? "null"
									: "'" + convierteFecha(credito.getFechaPago()) + "'");
							sql.append(",saldo_insoluto  = ");
							sql.append(credito.getSaldoInsolutoMO());
							sql.append(",codigo_mora = ");
							sql.append(credito.getCodigoMora());
							sql.append(",fecha_vencimiento = ");
							sql.append(credito.getFechaVencimiento() == null ? "null"
									: "'" + convierteFecha(credito.getFechaVencimiento()) + "'");
							sql.append(" where id_curse = ");
							sql.append(id_curse);
							sql.append(" and id_credito = ");
							sql.append(credito.getOpIFI());
							sql.append(" and numero_cuota = ");
							sql.append(credito.getCuotaQueCancela());

							System.out.println(sql);

							stmt.executeUpdate(sql.toString());
							this.cantidadRegistrosActualizados++;

						} catch (ParseException pex) {
							String datos = "Fecha Pago: " + credito.getFechaPago() + ", id_credito: "
									+ credito.getOpIFI() + ", Cuota que Cancela: " + credito.getCuotaQueCancela();
							throw new ParseException(datos, 0);
						}
					} else {
						try {
							StringBuffer s = new StringBuffer();

							s.append(
									"INSERT INTO pagos (id_credito,id_curse,fecha_vencimiento,fecha_pago,numero_cuota,monto_pago,saldo_insoluto,codigo_mora)");
							s.append(" VALUES (");
							s.append(credito.getOpIFI());
							s.append(",");
							s.append(id_curse);
							s.append(",");
							s.append(credito.getFechaVencimiento() == null ? "null"
									: "'" + convierteFecha(credito.getFechaVencimiento()) + "'");
							s.append(",");
							s.append(credito.getFechaPago() == null ? "null"
									: "'" + convierteFecha(credito.getFechaPago()) + "'");
							s.append(",");
							s.append(credito.getCuotaQueCancela());
							s.append(",");
							s.append(credito.getMontoPago());
							s.append(",");
							s.append(credito.getSaldoInsolutoMO());
							s.append(",");
							s.append(credito.getCodigoMora());
							s.append(")");

							System.out.println(s);

							stmt.executeUpdate(s.toString());
							this.cantidadRegistrosInsertados++;
						} catch (ParseException pex) {
							String datos = "Fecha Pago: " + credito.getFechaPago() + ", id_credito: "
									+ credito.getOpIFI() + ", Cuota que Cancela: " + credito.getCuotaQueCancela();
							throw new ParseException(datos, 0);
						}
					}

				}
				this.fila++;
				// c.commit();

			}
		} catch (SQLException e) {
			error = new ErrorSql(this.fila, e,
					"Error en la inserción o actualización de algun registro o problemas con la coneccion");
		} catch (ParseException pex) {
			String descripcion = "Se trato de convertir la fecha de pago a un registro valido para la BD y no fue posible - "
					+ pex.getMessage();
			error = new ErrorSql(this.fila, pex, descripcion);
		} catch (ClassNotFoundException cnex) {
			error = new ErrorSql(this.fila, cnex, "No encontré el driver de conexion a la base de datos Postgres");
		} catch (IOException ioex) {
			error = new ErrorSql(this.fila, ioex, "Problemas al leer el archivo de propiedades");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (c != null)
					c.close();
			} catch (SQLException e) {
				error = new ErrorSql(this.fila, e, "Error en la base de datos desconocido.");
			}
		}
	}

	/**
	 * Cantidad de registros efectivamente insertados
	 * 
	 * @return
	 */
	public int getCantidadRegistrosInsertados() {
		return this.cantidadRegistrosInsertados;
	}

	/**
	 * Cantidad de registros efectivamente actualizados
	 * 
	 * @return
	 */
	public int getCantidadRegistrosActualizados() {
		return this.cantidadRegistrosActualizados;
	}

	/**
	 * Contiene la información del error por excepcion.
	 * 
	 * @return Clase/Objeto ErrorSql
	 */
	public ErrorSql getError() {
		return this.error;
	}
}
