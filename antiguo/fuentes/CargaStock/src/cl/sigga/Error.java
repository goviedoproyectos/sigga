package cl.sigga;

/**
 * Captura datos en la línea de error del CSV
 * @author Gonzalo Oviedo
 *
 */
public class Error {
	private int numeroLinea;
	private String campo;
	private String explicacion;
	private Throwable ex;
	
	public Error(int numeroLinea, String campo, String explicacion, Throwable e) {
		this.numeroLinea = numeroLinea;
		this.campo = campo;
		this.ex = e;
		this.explicacion = explicacion;
	}
	
	public String getExplicacion() {
		return explicacion;
	}

	public void setExplicacion(String explicacion) {
		this.explicacion = explicacion;
	}

	public Throwable getEx() {
		return ex;
	}

	public void setEx(Throwable ex) {
		this.ex = ex;
	}

	public int getNumeroLinea() {
		return numeroLinea;
	}
	public void setNumeroLinea(int numeroLinea) {
		this.numeroLinea = numeroLinea;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	
	@Override
	public String toString() {
		String e = "\nListado de errores";
		e +="\n--------------------";
		e +="\nNumero de Linea: "+(this.numeroLinea+1);
		e +="\nCampo: "+this.campo;
		e +="\nExplicación: "+this.explicacion;
		e +="\nDescripción Técnica del error:"+this.ex.getMessage();
		return e;
	}
	
	
}
