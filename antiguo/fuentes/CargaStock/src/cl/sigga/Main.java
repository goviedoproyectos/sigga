package cl.sigga;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import cl.sigga.cargastock.excel.Excel;

/**
 * Punto de Partida
 * @author Gonzalo Oviedo
 */
public class Main {

	public static void main(String[] args) {
		
		Collection<Credito> creditos;
		//List<Error> errores;
		//Csv csv = new Csv();
		String fechaDeHoy = "";
		String archivo = "";
		Operaciones op = null;
		try {
			
			Date fechaHoy = new Date();
			
			//operaciones = csv.leeArchivo("/home/goviedo/proyectos/sigga/stock.csv");
			//operaciones = csv.leeArchivo(archivo);
			//errores = csv.getErrores();
			
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			fechaDeHoy = df.format(fechaHoy);
			archivo = "/var/www/sigga/sites/default/files/cargas/carga_stock_"+fechaDeHoy+"/stock.xlsx";
			//archivo = "stock.xlsx"; //Depuración.
			
			df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
			System.out.println("Hoy ("+df.format(fechaHoy)+") : "+archivo);
			
			Excel e = new Excel(archivo,Constantes.HOJA_EXCEL);
			creditos = e.getHojaExcel();
			
			op = new Operaciones(creditos);
			op.insertarOperaciones();
			
			ErrorSql err = op.getError();
			if(err!=null) {
				StringBuffer s = new StringBuffer();
				s.append(err.getComentario());
				s.append(":");
				s.append(err.getExcepcion().getMessage());
				s.append("Ultima Fila: ");
				s.append(err.getUltimaFilaInsertada());
				op.insertarComentarioInformeCargas(s.toString());
				System.out.println(s.toString());
			} else {
				//Insertamos OK, informe carga.
				op.insertarComentarioInformeCargas("REALIZADO. Insertados: "+op.getCantidadRegistrosInsertados()+". Actualizados: "+op.getCantidadRegistrosActualizados());
			}
		} catch(Exception e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
			try {
				op = new Operaciones(null);
				op.insertarComentarioInformeCargas("Error: "+e.getMessage());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		
	}

}
