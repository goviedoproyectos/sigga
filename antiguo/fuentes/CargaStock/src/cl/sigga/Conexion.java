package cl.sigga;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Gestiona las conexión a la bd.
 * @author Gonzalo Oviedo L.
 *
 */
public class Conexion {
	
	private Connection c;

	public Conexion() throws ClassNotFoundException, SQLException, IOException {
		
		Propiedades p = new Propiedades();
		c = null;
		
		Class.forName("org.postgresql.Driver");
		c = DriverManager.getConnection("jdbc:postgresql://"+p.getIp()+":"+p.getPuerto()+"/"+p.getNombreBd(), p.getUsuario(), p.getPassword());
	}
	
	/**
	 * Obtiene conexión
	 * @return
	 */
	public Connection getConexion() {
		return this.c;
	}
}
