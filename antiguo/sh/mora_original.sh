psql sigga <<EOF
update curse
set mora = (SELECT (case when ((current_date - fecha_vencimiento) > 0 ) then (current_date - fecha_vencimiento)else 0 end) suma
FROM tabla_desarrollo
WHERE (fecha_pago is null or fecha_pago::varchar = '0001-01-01 BC') and tipo_asociado = 1 and id_asociado = curse.id_curse
order by numero_cuota asc limit 1)
EOF
