do $$
declare
	p char(128);
begin
  select password into p from users where uid = 2;
  
  update users
  set password = p
  where uid = 1;
  
  --SELECT *
  --FROM   dbo.PubLists
  --WHERE  Name = v_List;
end $$;


select * from users;