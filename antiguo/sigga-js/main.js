var httpModule = require('./modules/http-module');

var http = require('http');
var port = 8081;
var servidor = '127.0.0.1';

http.createServer(httpModule.handle_request).listen(port,servidor);


console.log('Started Node.js http server at http://'+servidor+':' +   port); 
