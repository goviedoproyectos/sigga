CREATE OR REPLACE FUNCTION public.lst_dias_cambio_estado_bitacora_cobranza(integer)
 RETURNS SETOF type_dias_cobranza_estados
 LANGUAGE sql
AS $function$
select 
	c.id_cobranza::integer,
	c.id_curse::integer,
	CASE WHEN to_char(g2.fecha,'DD-MM-YYYY')!='' THEN 
	max(ceiling ((EXTRACT (epoch from g2.fecha) - EXTRACT(epoch from g1.fecha))/86400)::integer )
	ELSE
	max(ceiling (EXTRACT (epoch from now()-g1.fecha)/86400)::integer + cur.mora)
	END as DiasCobranza,

	CASE WHEN to_char(g3.fecha,'DD-MM-YYYY')!='' THEN 
	max(ceiling ((EXTRACT (epoch from g3.fecha) - EXTRACT(epoch from g2.fecha))/86400)::integer )
	ELSE
	max(ceiling (EXTRACT (epoch from now()-g2.fecha)/86400)::integer)
	END as DiasDemandaIngresada,	

	CASE WHEN to_char(g4.fecha,'DD-MM-YYYY')!='' THEN 
	max(ceiling ((EXTRACT (epoch from g4.fecha) - EXTRACT(epoch from g3.fecha))/86400)::integer )
	ELSE
	max(ceiling (EXTRACT (epoch from now()-g3.fecha)/86400)::integer)
	END as DiasDemandaProveida,
	
	--max(ceiling (EXTRACT (epoch from now()-g1.fecha_traspaso)/86400)::integer) as DiasDemandaIngresada,
	--max(ceiling (EXTRACT (epoch from now()-g3.fecha_traspaso)/86400)::integer) as DiasDemandaProveida,
	max(ceiling (EXTRACT (epoch from now()-g5.fecha)/86400)::integer) as DiasNotificacion
		
from cobranza as c
left join curse as cur on cur.id_curse = $1 

left join hitos_judiciales_cobranza as g1
on 
c.id_cobranza = g1.id_cobranza
and g1.id_hito = 1
and g1.id_hito_cobranza = (
	SELECT max(id_hito_cobranza)
	from hitos_judiciales_cobranza
	where id_hito = 1
	--and subestado in (1,2,3)
	and id_cobranza = g1.id_cobranza
	)


/*left join gestion_judicial as g1 on c.id_curse = g1.id_curse
--and g1.estado = 2
and g1.subestado = 1
and g1.fecha_traspaso =(
	SELECT fecha_traspaso 
	from gestion_judicial 
	WHERE estado = 1
	--and subestado in (1,2,3)
	and id_curse = $1
	and id_gestion_judicial = (select max(id_gestion_judicial) from gestion_judicial where id_curse = $1 AND estado = 1 )
	group by id_curse, fecha_traspaso) */

left join hitos_judiciales_cobranza as g3
on 
c.id_cobranza = g3.id_cobranza
and g3.id_hito = 3
and g3.id_hito_cobranza = (
	SELECT max(id_hito_cobranza)
	from hitos_judiciales_cobranza
	where id_hito = 3
	--and subestado in (1,2,3)
	and id_cobranza = g3.id_cobranza
	)


/*left join gestion_judicial as g3 on c.id_curse = g3.id_curse
and g3.estado = 3
and g3.fecha_traspaso = (
	SELECT fecha_traspaso
	from gestion_judicial 
	where estado = 3 
	--and subestado in (1,2,3)
	and id_curse = $1 
	and id_gestion_judicial = (select max(id_gestion_judicial) from gestion_judicial where id_curse = $1 AND estado = 3 )
	group by id_curse, fecha_traspaso)*/


left join hitos_judiciales_cobranza as g2
on 
c.id_cobranza = g2.id_cobranza
and g2.id_hito = 2
and g2.id_hito_cobranza = (
	SELECT max(id_hito_cobranza)
	from hitos_judiciales_cobranza
	where id_hito = 2
	--and subestado in (1,2,3)
	and id_cobranza = g2.id_cobranza
	)


/*left join gestion_judicial as g2
on 
c.id_curse = g2.id_curse
and g2.estado = 2
and g2.fecha_traspaso = (
	SELECT fecha_traspaso
	from gestion_judicial 
	where estado = 2 
	--and subestado in (1,2,3)
	and id_curse = $1
	and id_gestion_judicial = (select max(id_gestion_judicial) from gestion_judicial where id_curse = $1 AND estado = 2 )
	group by id_curse, fecha_traspaso)*/

left join hitos_judiciales_cobranza as g4
on 
c.id_cobranza = g4.id_cobranza
and g4.id_hito = 4
and g4.id_hito_cobranza = (
	SELECT max(id_hito_cobranza)
	from hitos_judiciales_cobranza
	where id_hito = 4 
	--and subestado in (1,2,3)
	and id_cobranza = g4.id_cobranza
	)
	

/*left join gestion_judicial as g4
on 
c.id_curse = g4.id_curse
and g4.estado = 4
and g4.fecha_traspaso = (
	SELECT fecha_traspaso
	from gestion_judicial 
	where estado = 4 
	--and subestado in (1,2,3)
	and id_curse = $1
	and id_gestion_judicial = (select max(id_gestion_judicial) from gestion_judicial where id_curse = $1 AND estado = 4 )
	group by id_curse, fecha_traspaso)*/
	
left join hitos_judiciales_cobranza as g5
on 
c.id_cobranza = g5.id_cobranza
and g5.id_hito = 4
and g5.id_hito_cobranza = (
	SELECT max(id_hito_cobranza)
	from hitos_judiciales_cobranza
	where id_hito = 4 
	--and subestado in (1,2,3)
	and id_cobranza = g5.id_cobranza
	)
	
where c.id_curse = $1
group by c.id_curse, c.id_cobranza,g1.fecha,g3.fecha,g4.fecha,g2.fecha

 $function$
