CREATE OR REPLACE FUNCTION public.get_last_table_desarrollo(bigint)
 RETURNS SETOF ultimo_pago
 LANGUAGE sql
AS $function$

select 
	b.cuota -- valor cuota
	,a.cuotas_pagadas as numero_cuota -- cuotas pagadas
	--,a.id_curse
From
(
select 
	count(1) as cuotas_pagadas
	,sum(monto_pago)  -- No usado pero podria ser util
	,id_credito -- No usado pero podria ser util
	,id_curse
from PAGOS 
--where id_credito = 399611 
where id_curse = $1
and monto_pago <> 0 -- Cuotas que esten pagadas
group by id_credito,id_curse

) a  join 
(
select 
	td.cuota, 
	c.id_credito
from curse c inner 
	join tabla_desarrollo td on td.id_asociado = c.id_curse and td.tipo_asociado=1
inner join pagos p on c.id_curse = p.id_curse
--where c.id_credito = 399611
where c.id_curse = $1
limit 1
)  b
on a.id_credito = b.id_credito

$function$

--drop FUNCTION public.get_last_table_desarrollo(bigint)
-- Necesario para el retorno de la funcion.
--create type ultimo_pago as (cuota numeric, numero_cuota bigint);


