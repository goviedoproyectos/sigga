CREATE OR REPLACE FUNCTION public.exportar_sabana()
 RETURNS SETOF type_exportar_sabana
 LANGUAGE sql
AS $function$

select
(SELECT alias_usuario FROM usuario WHERE id_usuario = c.ejecutivo)"Nombre Ejecutivo Sigga"
, c.ejecutivo_name "Nombre Ejecutivo Banco"
, c.razon_social_solicitante "Nombre / Razón Social"
, c.rut_solicitante::text||'-'||c.dv "Rut Solicitante"
, ae.descripcion "(código sbif) Actividad Económica"
, c.nombre_representante_legal_solicitante "Nombre Representante"
, CASE WHEN c.rut_representante_legal_solicitante != '' THEN c.rut_representante_legal_solicitante::text||'-'||c.dv_representante
	ELSE '' END "Rut Representante"
, CASE WHEN s.telefono IS NOT NULL THEN '+'||s.telefono::text ELSE '' END "Telefono"
, c.ventas_anuales "Ventas Anuales"
, cr.codigo_sbif "Clasificacion de Riesgo"
, s.direccion "Dirección"
, com.descripcion "Comuna"
, r.descripcion "Región"
, CASE WHEN s.es_exportador is null or s.es_exportador = '' THEN 'N' else s.es_exportador END "Es Exportador"
, c.id_credito "n° Operación"
, UPPER(gar.descripcion) "Tipo Crédito"
, prod.descripcion "Producto"
, oop.descripcion "Objetivo Operación"
, top.descripcion "Tipo Operación"
, replace(c.monto_operacion::text,'.',',') "Monto Crédito"
, mon.abreviatura "Moneda"
, replace(c.tasa_interes::text,'.',',') tasa
, c.plazo "Plazo"
, per.descripcion "Periodicidad"
, c.periodo_gracia "Periodo Gracia"
, c.cantidad_cuota "Cantidad de Cuotas"
, c.comision_banco "Comision Banco"
, c.cobertura "Cobertura"
, to_char(c.fecha_curse,'DD/MM/YYYY') "Fecha Curse"
, to_char(td.fecha_vencimiento, 'DD/MM/YYYY') "Fecha Vencimiento"
, to_char(td.fecha_vencimiento, 'DD/MM/YYYY') "Fecha Vencmiento Crédito" -- Mismo segun Paula Palacios
--, (SELECT to_char(fecha_vencimiento, 'DD/MM/YYYY') FROM tabla_desarrollo
--	WHERE id_asociado = c.id_curse and tipo_asociado = 1
--	ORDER BY numero_cuota DESC LIMIT 1) "Fecha Vencmiento Crédito"
, ce.descripcion "Estado"
, ga.desc_tipo "Garantia Adicional"
, (SELECT name_list FROM (SELECT id_curse ,string_agg('('||rut||'-'||dv||') '||nombre, ',') name_list FROM aval GROUP BY 1 ORDER BY 1 DESC) listado_avales
	WHERE id_curse = c.id_curse) "(Rut Aval)Garantias Personales"
, (SELECT descrip FROM 
	(SELECT id_curse, string_agg('($'||valor_comercial::text||') '||clase::text||validez::text||tipo::text||'-'||UPPER(desc_clase)||
	'-'||UPPER(desc_validez)||'-'||UPPER(desc_tipo), ',') descrip
	FROM clase_garantia_adicional,validez_garantia_adicional,tipo_garantias_adicionales, garantias_adicionales_curse WHERE id_curse = c.id_curse
	and (clase::text||validez::text||tipo::text = id_tipo_garantia)
	GROUP BY 1) listado WHERE id_curse = c.id_curse) "(Valor Comercial)Garantias Reales"
--, c.mora "Mora"
,CURRENT_DATE - td.fecha_vencimiento as mora
, (SELECT saldo_insoluto FROM tabla_desarrollo where id_asociado = c.id_curse and tipo_asociado = 1 and 
	(to_char(fecha_pago,'YYYY-MM-DD') = '' OR fecha_pago = '0001-01-01 BC' OR fecha_pago IS NULL) ORDER BY id_tabla_desarrollo ASC LIMIT 1) "Saldo Crédito"
, to_char(cob.fecha_cobranza, 'DD/MM/YYYY') "Fecha Traspaso Cobranza"
--, (SELECT to_char(fecha_vencimiento,'DD/MM/YYYY') FROM get_primera_cuota_impaga(c.id_curse) WHERE fecha_vencimiento < current_date) "Fecha Primera Cuota Impaga"
,to_char(pagos.fecha_vencimiento, 'DD/MM/YYYY') "Fecha Primera Cuota Impaga"
--,(select fecha_vencimiento from pagos where id_curse =  c.id_curse and monto_pago = 0) "Fecha Primera Cuota Impaga"
, (SELECT UPPER(abogado_asignado) FROM gestion_judicial where id_curse = c.id_curse ORDER BY id_gestion_judicial DESC LIMIT 1 )"Abogado/Estudio Asignado"
, (SELECT descripcion FROM gestion_judicial gj
	LEFT JOIN estados_gestion_judicial ec ON gj.estado = ec.id_estado_gestion
	WHERE gj.id_curse = c.id_curse
	ORDER BY gj.id_gestion_judicial DESC LIMIT 1 ) "Estado Cobranza"
, (SELECT descripcion FROM gestion_judicial gj
	LEFT JOIN subestados_gestion_judicial sub ON gj.subestado = sub.id_subestado_gestion
	WHERE gj.id_curse = c.id_curse
	ORDER BY gj.id_gestion_judicial DESC LIMIT 1 ) "Subestado Cobranza"
, (SELECT tribunal FROM gestion_judicial WHERE id_curse = c.id_curse ORDER BY id_gestion_judicial DESC LIMIT 1) tribunal
, (SELECT rol_causa FROM gestion_judicial WHERE id_curse = c.id_curse ORDER BY id_gestion_judicial DESC LIMIT 1) rol_causa
, (SELECT (365 - (CURRENT_DATE - td.fecha_vencimiento))) alerta_notif
, (SELECT (425 - (CURRENT_DATE - td.fecha_vencimiento))) alerta_gar_estatal
, (SELECT to_char(fecha_estado, 'DD-MM-YYYY') FROM op_cobro_garantia where tipo_operacion = 2 and id_curse = c.id_curse
	AND to_char(fecha_estado, 'DD-MM-YYYY') != '01-01-0001' order by id_opcobrogarantia DESC LIMIT 1) fecha_envio_cobro
,(SELECT to_char(fecha_estado, 'DD-MM-YYYY') FROM op_cobro_garantia where tipo_operacion = 4 and id_curse = c.id_curse
	AND to_char(fecha_estado, 'DD-MM-YYYY') != '01-01-0001' order by id_opcobrogarantia DESC LIMIT 1) fecha_ultima_apelacion
,(SELECT descripcion FROM estado_gestion_cobro where  
	id_estado_gestion_cobro = (SELECT tipo_operacion FROM op_cobro_garantia WHERE tipo_operacion in (5,6) AND id_curse = c.id_curse ORDER BY id_opcobrogarantia DESC LIMIT 1)
	) resolucion_pago
, (SELECT to_char(fecha_estado, 'DD-MM-YYYY') FROM op_cobro_garantia where tipo_operacion in (5,6) AND id_curse = c.id_curse ORDER BY id_opcobrogarantia DESC LIMIT 1) fecha_resolucion
, cb.monto_pagadomo
, cb.monto_deduciblemo
FROM curse c
LEFT JOIN solicitante s ON c.rut_solicitante = s.rut
LEFT JOIN tipo_actividad_economica ae ON s.actividad_economica = ae.id_actividad_economica
LEFT JOIN tipo_clasificacion_riesgo cr ON s.clasificacion_riesgo = cr.id_clasificacion_riesgo
LEFT JOIN region r ON s.region = r.id_region
LEFT JOIN comuna com ON s.comuna = com.id_comuna
LEFT JOIN tipo_garantia gar ON c.tipo_garantia = gar.id_tipo_garantia
LEFT JOIN tipo_producto prod ON c.producto = prod.id_producto
LEFT JOIN tipo_operacion_corfo top ON c.objetivo_operacion = top.codigo
LEFT JOIN tipo_objetivo_operacion oop ON c.tipo_operacion = oop.id_objetivo
LEFT JOIN tipo_moneda mon ON c.moneda = mon.id_moneda
LEFT JOIN tipo_periodo per ON c.periodo_pago = per.id_periodo
LEFT JOIN estados_curse ce ON c.id_estado = ce.id_estado_curse
LEFT JOIN tipo_garantias_adicionales ga ON c.garantia_adicional = ga.tipo
LEFT JOIN cobranza cob ON c.id_curse = cob.id_curse
left join cobro cb on cb.id_curse = c.id_curse
left join tabla_desarrollo td on td.id_asociado = c.id_curse
left join pagos pagos on pagos.id_curse = c.id_curse
where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos pagos where pagos.id_curse = c.id_curse)
and pagos.id_pago = (select max(id_pago) from pagos p where p.id_curse = c.id_curse)

$function$
