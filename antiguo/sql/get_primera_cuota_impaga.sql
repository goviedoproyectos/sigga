CREATE OR REPLACE FUNCTION public.get_primera_cuota_impaga(bigint)
 RETURNS SETOF saldo_insoluto
 LANGUAGE sql
AS $function$
--SELECT * FROM tabla_desarrollo
--WHERE id_asociado = $1 and tipo_asociado = 1 and (fecha_pago is NULL or fecha_pago = '0001-01-01 BC')
--ORDER BY id_tabla_desarrollo asc limit 1;
select cast(saldo_insoluto as bigint) as saldo_insoluto,fecha_vencimiento from pagos where id_curse =  $1 and monto_pago = 0
$function$

-- Esto fue el tipo de dato que tuvo que utilizarse para crear la funcion
create type saldo_insoluto as (saldo_insoluto bigint, fecha_vencimiento date);

