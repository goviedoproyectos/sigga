CREATE OR REPLACE FUNCTION  public.GET_DIAS_TRANSCURRIDOS_GE(bigint) returns int as $$
	SELECT (CURRENT_DATE - ocg.fecha_estado) as dias
	FROM op_cobro_garantia ocg
	WHERE ocg.id_curse = $1
	AND id_opcobrogarantia = (SELECT MAX(id_opcobrogarantia) from op_cobro_garantia where id_curse = $1) -- representa al ultimo estado en curso
$$ language 'sql';


