CREATE OR REPLACE FUNCTION public.get_mora (ID_CREDITO bigint)
RETURNS int
 LANGUAGE sql
AS $function$

		--select (CURRENT_DATE - td.fecha_vencimiento)::bigint
			--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse inner join curse c on td.id_asociado = c.id_curse
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_curse = p.id_curse) 
		--and p.id_curse = td.id_asociado
		--and c.id_credito = $1
		--order by td.fecha_vencimiento asc LIMIT 1

		--Al cambiar la Fecha de Vencimiento a la tabla pagos, se debio cambiar la consulta
		select CURRENT_DATE - fecha_vencimiento as mora
		from pagos   
		where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = $1) 
		and id_credito = $1
		and codigo_mora > 1
		order by fecha_vencimiento asc LIMIT 1
	--insert function body here. wrap in single quotes
	--	select ID_CREDITO
	--	LANGUAGE SQL;
	$function$


