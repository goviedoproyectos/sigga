CREATE OR REPLACE FUNCTION public.get_curse(bigint, bigint)
 RETURNS SETOF get_curse_solicitante
 LANGUAGE sql
AS $function$
select 
	c.rut_solicitante::bigint,
	c.dv, c.razon_social_solicitante, 
	c.rut_representante_legal_solicitante, 
	c.dv_representante, 
	c.nombre_representante_legal_solicitante,
	c.ventas_anuales,
	s.direccion,
	s.actividad_economica, 
	UPPER(ae.descripcion), 
	c.id_curse, 
	c.id_credito, 
	c.producto, 
	c.tipo_operacion, 
	c.tipo_garantia,
	c.moneda, 
	c.monto_operacion, 
	c.objetivo_operacion, 
	c.plazo, 
	c.periodo_pago, 
	c.periodo_gracia, 
	c.cantidad_cuota, 
	c.primer_vencimiento,
	c.fecha_curse, 
	c.comision_banco, 
	c.comision_corfo, 
	c.comision_sigga, 
	c.tasa_interes, 
	c.monto_total_gasto, 
	c.cobertura,
	c.periodo_aprobacion, 
	c.tipo_cuota, 
	c.mora, 
	c.fecha_referecia_alerta_mora, 
	c.fecha_referencia_alerta_reparo, 
	c.capital_residual,
	c.id_estado, 
	c.id_tabla_desarrollo, 
	c.reparos, 
	c.inversion_activo_fijo,
	c.capital_trabajo, 
	c.refinanciamiento, 
	c.linea_factoring, 
	c.linea_credito,
	c.ejecutivo, 
	c.tipo_empresa,
	c.id_credito_ant,
	c.id_certificado, 
	c.reprogamado, 
	s.telefono, 
	s.direccion_representante,
	s.telefono_representante, 
	te.descripcion,
	c.garantia_adicional, 
	c.aplicativo_fogape, 
	c.cuoton
from curse c
left join solicitante s on c.rut_solicitante = s.rut
left join tipo_actividad_economica ae on s.actividad_economica = ae.id_actividad_economica
left join tipo_empresa te on c.tipo_empresa = te.id_tipo_empresa
 where c.id_curse = (case when $1 =-1  then c.id_curse else $1::bigint end ) and
 c.rut_solicitante = (case when $2 =-1 then c.rut_solicitante else $2::bigint end )
 order by c.id_curse desc
$function$
