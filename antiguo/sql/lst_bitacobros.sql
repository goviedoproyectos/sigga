CREATE OR REPLACE FUNCTION public.lst_bitacobros()
 RETURNS SETOF type_bitacobros
 LANGUAGE sql
AS $function$

SELECT cob.id_curse, o.tipo_operacion, o.fecha_estado, o.fecha_primer_vencimiento_impago, c.razon_social_solicitante as descripcion, c.id_credito, c.rut_solicitante, c.dv, c.razon_social_solicitante, 
c.fecha_curse, 

--c.mora,

--(
--		select CURRENT_DATE - td.fecha_vencimiento as mora  
--		from tabla_desarrollo td inner join pagos p on td.id_asociado = c.id_curse 
--		where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = c.id_credito) 
--		and p.id_curse = c.id_curse
--		order by td.fecha_vencimiento asc LIMIT 1
--		) as mora,

--Al cambiar la Fecha de Vencimiento a la tabla pagos, se debio cambiar la consulta
(
		select CURRENT_DATE - fecha_vencimiento as mora
		from pagos   
		where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = c.id_credito) 
		and id_credito = c.id_credito
		and codigo_mora > 1
		order by fecha_vencimiento asc LIMIT 1
) as mora,


(select descripcion from tipo_garantia where id_tipo_garantia = c.tipo_garantia) as garantia, 
(select descripcion from tipo_producto where id_producto = c.producto) as descripcion_producto

FROM 
	op_cobro_garantia o, 
--	estado_gestion_cobro e, -- Esta tabla repite las filas. El tipo de Operacion cuando es -1, no muestra la fila
	curse c, 
	cobro cob 
WHERE 
--(o.tipo_operacion = e.id_estado_gestion_cobro) --or cob.estado_gestion_cobro = -1
cob.id_curse = c.id_curse 
AND o.id_opcobrogarantia in (SELECT MAX(id_opcobrogarantia) from op_cobro_garantia where id_curse = c.id_curse) -- representa al ultimo estado en curso
ORDER BY cob.id_curse DESC
$function$
