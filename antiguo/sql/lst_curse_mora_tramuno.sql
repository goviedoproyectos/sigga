CREATE OR REPLACE FUNCTION public.lst_curse_mora_tramuno(bigint, bigint, bigint, bigint, character varying, numeric, numeric, numeric, numeric, numeric, integer)
 RETURNS SETOF get_encabezado_curse
 LANGUAGE sql
AS $function$
select 	c.id_curse,
		c.id_credito,
		c.rut_solicitante,
		c.dv,
		c.razon_social_solicitante,
		p.descripcion,
		c.producto,
		g.descripcion,
		c.tipo_garantia,
		e.descripcion,
		c.reprogamado,
		ec.descripcion,
		c.id_estado,
		c.estado_certificacion,
		
		--(
		--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse 
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = c.id_credito) 
		--and p.id_curse = c.id_curse
		--order by td.fecha_vencimiento asc LIMIT 1
		--),
		
		--Al cambiar la Fecha de Vencimiento a la tabla pagos, se debio cambiar la consulta
		(
		select CURRENT_DATE - fecha_vencimiento
		from pagos   
		where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = c.id_credito) 
		and id_credito = c.id_credito
		and codigo_mora > 1
		and id_curse = c.id_curse
		order by fecha_vencimiento asc LIMIT 1
		) as mora,

		
		0::numeric,
		c.monto_operacion, 
		c.moneda, 
		c.fecha_curse
  from curse c
    inner join tipo_producto p on p.id_producto=c.producto
  inner join tipo_garantia g on g.id_tipo_garantia=c.tipo_garantia
  inner join estados_curse e on e.id_estado_curse=c.id_estado 
  right join estado_certificacion ec on ec.id_estado=c.estado_certificacion
  where e.descripcion !='En Cobranza'
    ORDER BY id_curse DESC
$function$
