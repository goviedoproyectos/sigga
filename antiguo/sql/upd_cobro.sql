CREATE OR REPLACE FUNCTION public.upd_cobro(bigint, numeric, numeric, numeric, character varying, integer, numeric, numeric, numeric,numeric,numeric)
 RETURNS SETOF cobro
 LANGUAGE sql
AS $function$
UPDATE cobro
   SET 
   	monto_pagadomo 			= $2, 
   	monto_aprobadomo 		= $3, 
   	monto_deduciblemo 		= $4,
   	fecha_resolucion_final 	= to_date($5,'YYYYMMDD'),
   	estado_gestion_cobro 	= $6,
   	monto_pagado 			= $7, 
   	monto_aprobado 			= $8, 
   	monto_deducible 		= $9,
   	euro = $10,
   	dolar = $11
   	--estado_gestion_cobro 	= CASE WHEN $6 = 1 THEN 6 WHEN $6 = 2 THEN 5 ELSE estado_gestion_cobro END
 WHERE id_cobranza = $1
 RETURNING *;
$function$