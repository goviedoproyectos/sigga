CREATE OR REPLACE FUNCTION public.lst_bitacora_cobranza(bigint, bigint, bigint, bigint, character varying, numeric, numeric, numeric, integer, integer, integer)
 RETURNS SETOF type_bitacora_cobranza
 LANGUAGE sql
AS $function$
select 	
		cu.id_curse,
		cu.id_credito,
		cu.rut_solicitante,
		cu.dv,
		cu.razon_social_solicitante,
		p.descripcion,
		g.descripcion,
		--mora,
		--(
		--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse 
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = cu.id_credito) 
		--and p.id_curse = cu.id_curse
		--order by td.fecha_vencimiento asc LIMIT 1
		--),
		(
			select CURRENT_DATE - fecha_vencimiento 
			from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = cu.id_credito ) 
			and id_credito = cu.id_credito
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1
		) as mora,
		co.estado,
		eco.descripcion,
		cu.id_estado,
		co.numero_notificaciones, 
		co.ley_quiebra
from curse cu
left outer join cobranza co on co.id_curse=cu.id_curse 
left outer join estados_cobranza eco on eco.id_estado_cobranza=co.estado
  inner join tipo_garantia g on g.id_tipo_garantia=cu.tipo_garantia
  inner join tipo_producto p on p.id_producto=cu.producto
  inner join gestion_judicial gj on gj.id_curse = cu.id_curse -- Gonzalo Oviedo. Solo 3 primeros estados.
where cu.id_estado=10 
and gj.estado in (1,2,3) -- Gonzalo Oviedo Solo muestre los 3 primeros estados. 1 INGRESO COBRANZA JUDICIAL 2 DEMANDA PRESENTADA 3 DEMANDA PROVEIDA. Notificacion hacia adelante no.
AND gj.id_gestion_judicial = (SELECT MAX(id_gestion_judicial) FROM gestion_judicial WHERE id_curse = cu.id_curse) -- Gonzalo Oviedo. Maximo id de gestion judicial que contiene el ultimo estado.
and cu.id_credito = (case when $1 != -1 then $1 else cu.id_credito end )
and cu.rut_solicitante = (case when $2!= -1 then $2 else cu.rut_solicitante end)
--AND co.estado  = (CASE WHEN $3!= -1 THEN $3 ELSE co.estado  end) 
and cu.id_curse = (case when $4!= -1 THEN $4 ELSE cu.id_curse end)
and cu.razon_social_solicitante = (case when $5 NOT LIKE '' THEN $5 ELSE cu.razon_social_solicitante end)
and (case when $9=0 then cu.ejecutivo=$8 else cu.ejecutivo=cu.ejecutivo end)
--and cu.ejecutivo in (SELECT id from get_ejecutivos_id($8,$9))  
and  CASE WHEN $11 = -1 THEN (
		--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse 
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = cu.id_credito) 
		--and p.id_curse = cu.id_curse
		--order by td.fecha_vencimiento asc LIMIT 1
		
		select CURRENT_DATE - fecha_vencimiento 
			from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = cu.id_credito ) 
			and id_credito = cu.id_credito
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1

		) > $10 ELSE (
		--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse 
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = cu.id_credito) 
		--and p.id_curse = cu.id_curse
		--order by td.fecha_vencimiento asc LIMIT 1
		
		select CURRENT_DATE - fecha_vencimiento 
			from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = cu.id_credito ) 
			and id_credito = cu.id_credito
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1
		
		) > $10 and (
		--select CURRENT_DATE - td.fecha_vencimiento as mora  
		--from tabla_desarrollo td inner join pagos p on td.id_asociado = p.id_curse 
		--where td.fecha_vencimiento > (select MAX(fecha_pago) from pagos where id_credito = cu.id_credito) 
		--and p.id_curse = cu.id_curse
		--order by td.fecha_vencimiento asc LIMIT 1
		
		select CURRENT_DATE - fecha_vencimiento 
			from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_credito = cu.id_credito ) 
			and id_credito = cu.id_credito
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1
		
		) <= $11 end
group by cu.id_curse,
		cu.id_credito,
		cu.rut_solicitante,
		cu.dv,
		cu.razon_social_solicitante,
		p.descripcion,
		g.descripcion,
		co.estado,
		eco.descripcion,
		cu.id_estado,
		co.numero_notificaciones, 
		co.ley_quiebra,
		co.id_cobranza,
		gj.id_gestion_judicial
ORDER BY co.id_cobranza DESC
limit $7 offset $6
 $function$
 
 
