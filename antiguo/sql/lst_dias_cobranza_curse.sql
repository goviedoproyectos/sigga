CREATE OR REPLACE FUNCTION public.lst_dias_cobranza_curse(integer)
 RETURNS SETOF type_dias_cobranza_estados as $$
 declare e int4;
 begin
	select estado into e from gestion_judicial 
	where id_curse = $1 
	order by id_gestion_judicial desc limit 1;
	
	IF e = 1 THEN
    RETURN QUERY
    
	    select
	    	0,
	    	0,
	    	CURRENT_DATE - fecha_vencimiento as DiasCobranza,
	    	0 as DiasDemandaIngresada,
	    	0 as DiasDemandaProveida,
	    	0 as DiasNotificacion
	    	from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_curse = $1) 
			and id_curse = $1
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1;
  	END if;
  	
  	IF e = 2 THEN
    RETURN QUERY
    
	    select
	    	0,
	    	0,
	    	0 as DiasCobranza,
	    	CURRENT_DATE - fecha_vencimiento as DiasDemandaIngresada,
	    	0 as DiasDemandaProveida,
	    	0 as DiasNotificacion
	    	from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_curse = $1) 
			and id_curse = $1
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1;
  	END if;
  	
  	IF e = 3 THEN
    RETURN QUERY
    
	    select
	    	0,
	    	0,
	    	0 as DiasCobranza,
	    	0 as DiasDemandaIngresada,
	    	CURRENT_DATE - fecha_vencimiento as DiasDemandaProveida,
	    	0 as DiasNotificacion
	    	from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_curse = $1) 
			and id_curse = $1
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1;
  	END if;
  	
  	IF e = 4 THEN
    RETURN QUERY
    
	    select
	    	0,
	    	0,
	    	0 as DiasCobranza,
	    	0 as DiasDemandaIngresada,
	    	0 as DiasDemandaProveida,
	    	CURRENT_DATE - fecha_vencimiento as DiasNotificacion
	    	from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_curse = $1) 
			and id_curse = $1
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1;
  	END if;
  	
  	IF e > 4 or e < 1 THEN
    RETURN QUERY
    
	    select
	    	0,
	    	0,
	    	0 as DiasCobranza,
	    	0 as DiasDemandaIngresada,
	    	0 as DiasDemandaProveida,
	    	0 as DiasNotificacion
	    	from pagos   
			where fecha_vencimiento > (select MAX(coalesce(fecha_pago, '17770101')) from pagos where id_curse = $1) 
			and id_curse = $1
			and codigo_mora > 1
			order by fecha_vencimiento asc LIMIT 1;
  	END if;
  	
  end;
  $$
LANGUAGE plpgsql 
